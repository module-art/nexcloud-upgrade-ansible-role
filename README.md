Upgrade or install Nexcloud in Docker
=========

That role can be used to install or upgrade nextcloud with Docker.
It make a custom image based on nextcloud-apache with some additionnal features (see in template/Dockerfile.j2).
And run a docker compose stack with mariadb and redis linked.

Requirements
------------

Tested Docker version 26.1.2

community.docker collection version 3.9.0

ansible core 2.14.3 or later

Works as well with docker-compose package or docker compose plugin

Role Variables
--------------

See all variables in defaults/main.yml

# Important to set:

nextcloud_image_tag: <version>-apache
custom_nextcloud_image: your_project/nextcloud-ssl

# Defaults :

use_docker_compose_v2: true #Set to false for using docker-compose package
sleep_duration: 6 #Time to wait (s) between maintenance mode and stopping containers

#Default directories for files and data 
nextcloud_dir: nextcloud
nextcloud_path: "docker/{{ nextcloud_dir }}"
data_path: "/home/{{ ansible_user }}/{{ nextcloud_path }}"

#Vars above should be set in a group_vars vault file
nextcloud_admin_password: secret
nextcloud_admin_user: admin
nextcloud_trusted_domains: "nextcloud.test"

mysql_root_password: secret
mysql_password: secret
redis_password: "{{ mysql_password }}"

mysql_database: nextcloud
mysql_user: nextcloud

nextcloud_published_port: 8080
nextcloud_tls_published_port: 4430

#Set volume for tls certs, example: "- /etc/letsencrypt:/etc/letsencrypt"
lets_encrypt_path: ""

#Custom extra config must be a list of items for each line
extra_nextcloud_config: ""

Dependencies
------------

No dependencies

Example Playbook
----------------
    - name: docker nextcloud upgrade with ansible
      hosts: your_hosts
      roles:
      - nextcloud-upgrade

License
-------

BSD

Author Information
------------------

Sylvestre Lambey - 2024
